#!/bin/bash -p
# Functions and aliases used to nagivate project directories.
# Source this file to add the named function to you BASH environment.
#############################################################################

# -------------------------------------------------------------------
# BeagleBox: Embedded environment for OMAP-based systems.
# -------------------------------------------------------------------
function beaglebox {
    # Project ID
    PRJ=beaglebox

	# GIT Repo
	export GITREPO=git@gitlab.com:beaglebox/beaglebox.git

    # Top of the source tree
	# The source, build, archive and package directories will live
	# under this tree.
    SRCTOP=<SET A PATH HERE>

    # Where I do my dev work
    GM_WORK=$SRCTOP/work
    # Where the CVS is located
    GM_HOME=$SRCTOP/$PRJ
    # Where the source and build directories live
    GM_SRC=$GM_HOME/src
    GM_BUILD=$GM_HOME/bld
    GM_ARCHIVE=$GM_HOME/archive
    GM_PKG=$GM_HOME/pkg

	# Where is the AM35x/OMAP35x SGX SDK Installed
	# We use this to grab SGX binary libraries, if needed.
	GM_SGX_DIR=$GM_HOME/extras/sdk

    # Make the configured environment available to the build system.
    export GM_WORK
    export GM_ARCHIVE
    export GM_PKG
    export GM_HOME
    export GM_SRC
    export GM_BUILD

    # Some aliases to bounce around directories easily
    alias cdt='cd $SRCTOP'
    alias cdh='cd $GM_HOME'
    alias cdw='cd $GM_WORK'
    alias cdx='cd $GM_SRC'
    alias cdb='cd $GM_BUILD'
    alias cda='cd $GM_ARCHIVE'
    alias cdp='cd $GM_PKG'

    # Show the aliases for this configuration
    alias cd?='cdbb?'
    alias cvsu='cvs update 2>&1 | egrep "^[UPAMRC\?]"'
}
function cdbb? {
echo "
BeagleBox Alias settings:
-----------------------------------------------------------------------------
cdt    cd SRCTOP ($SRCTOP)
cdh    cd GM_HOME ($GM_HOME)
cdw    cd GM_WORK ($GM_WORK)
cdx    cd GM_SRC ($GM_SRC)
cdb    cd GM_BUILD ($GM_BUILD)
cda    cd GM_ARCHIVE ($GM_ARCHIVE)
cdp    cd GM_PKG ($GM_PKG)

To checkout tree:
cdt
mkdir $PRJ
cdh
git clone ssh://$GITREPO src
"
}

