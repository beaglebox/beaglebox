# Build System For Embedded Boards - builds buildroot 
# (cross compiler, filesystem, busybox) and uboot.
# ----------------------------------------------------
all: defaultBB

# These files contain common variables and targets.
include config.mk
include util.mk

# ---------------------------------------------------------------
# Default build
# This should create a default BeagleBox distribution.  
# ---------------------------------------------------------------
defaultBB: $(XCC_T) $(UBOOT_T) $(KERNEL_T) $(BUILDROOT_T) $(PKG_T)

# ---------------------------------------------------------------
# Cleanup targets - seldom used since they affect all 
# components at once.
# ---------------------------------------------------------------
clean: 
	@for component in $(TARGETS); do \
		echo "Clobbering: $$component"; \
		make --no-print-directory $$component-clean; \
	done

# Careful - this wipes your archive out too!
clobber: 
	@for component in $(TARGETS); do \
		echo "Clobbering: $$component"; \
		make --no-print-directory $$component-clobber; \
	done
	@rm -rf $(ARCDIR) $(BLDDIR) 

