# ---------------------------------------------------------------
# Build the bootloader: u-boot 
# Set HW to the name of the board to build for.
# ---------------------------------------------------------------

# Retrieve bootloader patches, if any
.$(UBOOT_T)-get-patches $(UBOOT_T)-get-patches: 
	@if [ "x$(UBOOT_PATCH_URL)" != "x" ]; then \
		if [ ! -f $(UBOOT_ARCDIR)/$(UBOOT_PATCH) ]; then \
			$(MSG) "================================================================"; \
			$(MSG3) Retrieving u-boot patches $(EMSG); \
			$(MSG) "================================================================"; \
			D=$(UBOOT_ARCDIR) S=$(UBOOT_PATCH) U=$(UBOOT_PATCH_URL) make --no-print-directory getsw-only; \
		else \
			$(MSG3) u-boot patches are cached $(EMSG); \
		fi; \
	else \
		$(MSG3) No patches configured for u-boot $(EMSG); \
	fi
	@touch .$(subst .,,$@)

# Retrieve bootloader package
.$(UBOOT_T)-get $(UBOOT_T)-get: .$(UBOOT_T)-get-patches 
	@mkdir -p $(BLDDIR) $(UBOOT_ARCDIR)
	@if [ ! -d $(UBOOT_ARCDIR)/$(UBOOT_CLONEDIR) ]; then \
		$(MSG) "================================================================"; \
		$(MSG3) Retrieving u-boot source $(EMSG); \
		$(MSG) "================================================================"; \
		cd $(UBOOT_ARCDIR) && git clone $(UBOOT_URL) $(UBOOT_ARCDIR)/$(UBOOT_CLONEDIR); \
		cd $(UBOOT_ARCDIR)/$(UBOOT_CLONEDIR) && git checkout $(UBOOT_BRANCH); \
	else \
		$(MSG3) u-boot source is cached $(EMSG); \
		cd $(UBOOT_ARCDIR)/$(UBOOT_CLONEDIR) && git checkout $(UBOOT_BRANCH); \
	fi
	@touch .$(subst .,,$@)

# Unpack the bootloader.  Since it's a git clone, we just copy it.
.$(UBOOT_T)-unpack $(UBOOT_T)-unpack: .$(UBOOT_T)-get
	@if [ ! -d $(UBOOT_SRCDIR) ]; then \
		if [ -d $(UBOOT_ARCDIR)/$(UBOOT_CLONEDIR) ]; then \
			$(MSG3) "Unpacking u-boot source" $(EMSG); \
			cp -r $(UBOOT_ARCDIR)/$(UBOOT_CLONEDIR) $(BLDDIR); \
		else \
			$(MSG11) "u-boot source archive is missing:" $(EMSG); \
			$(MSG11) "$(UBOOT_ARCDIR)/$(UBOOT_CLONEDIR) not found " $(EMSG); \
			exit 1; \
		fi; \
	fi
	@touch .$(subst .,,$@)

.$(UBOOT_T)-unpack-patches $(UBOOT_T)-unpack-patches: .$(UBOOT_T)-unpack
	@if [ "x$(UBOOT_PATCH_URL)" != "x" ]; then \
		mkdir -p $(UBOOT_SRCDIR)/patches; \
		tar -C $(UBOOT_SRCDIR)/patches -$(UBOOT_PATCH_JZ)xf $(UBOOT_ARCDIR)/$(UBOOT_PATCH); \
	fi
	@touch .$(subst .,,$@)

# Patch the bootloader, if necessary.
.$(UBOOT_T)-patch $(UBOOT_T)-patch: .$(UBOOT_T)-unpack-patches
	@if [ "x$(UBOOT_PATCHDIR)" != "x" ]; then \
		if [ -d $(UBOOT_PATCHDIR) ]; then \
			for patchname in `ls -1 $(UBOOT_PATCHDIR)`; do \
				if [ "$$patchname" != "CVS" ]; then \
					$(MSG3) Applying $$patchname $(EMSG); \
					cd $(UBOOT_SRCDIR) && patch -Np1 -r - < $(UBOOT_PATCHDIR)/$$patchname; \
				fi; \
			done; \
		else \
			$(MSG11) "u-boot patches not found" $(EMSG); \
			$(MSG11) "Patch dir: $(UBOOT_PATCHDIR)" $(EMSG); \
			exit 1; \
		fi; \
	else \
		$(MSG3) "u-boot source does not need to be patched." $(EMSG); \
	fi
	@touch .$(subst .,,$@)

# Setup for the bootloader build
.$(UBOOT_T)-init $(UBOOT_T)-init: .$(UBOOT_T)-patch
	@touch .$(subst .,,$@)

# Copy in the local copy of the u-boot configuration
$(UBOOT_T)-preconfig: 
	@if [ -f $(DIR_UBOOT)/$(UBOOT_CONFIG_SRC) ]; then \
		$(MSG6) "Using local configuration file: $(UBOOT_CONFIG_SRC)" $(EMSG); \
		cp $(DIR_UBOOT)/$(UBOOT_CONFIG_SRC) $(UBOOT_SRCDIR)/include/configs/$(UBOOT_CONFIG); \
	else \
		$(MSG6) "No local configuration file for $(UBOOT_T)" $(EMSG); \
	fi

$(UBOOT_T)-verify:
	@if [ "x$(XCC_PREFIXDIR)" = "x" ]; then \
		$(MSG11) "Missing XCC_PREFIXDIR" $(EMSG); \
		$(MSG11) "Try setting XI on the command line." $(EMSG); \
		exit 1; \
	fi
	@if [ ! -d $(XCC_PREFIXDIR) ]; then \
		$(MSG11) "No such directory: $(XCC_PREFIXDIR)" $(EMSG); \
		$(MSG11) "Try setting XI on the command line." $(EMSG); \
		exit 1; \
	fi
	
# Build the bootloader
$(UBOOT_T): .$(UBOOT_T)

.$(UBOOT_T): .$(UBOOT_T)-init 
	@make $(UBOOT_T)-verify
	@$(MSG) "================================================================"
	@$(MSG2) "Building Bootloader (u-boot)" $(EMSG)
	@$(MSG) "================================================================"
	@$(MSG6) "Building for $(HW)" $(EMSG)
	@make --no-print-directory $(UBOOT_T)-preconfig
	@cd $(UBOOT_SRCDIR) && PATH=$(UBOOT_PATH) CROSS_COMPILE=$(CROSS_COMPILER) \
		make ARCH=$(ARCH) omap3_beagle_config
	@cd $(UBOOT_SRCDIR) && PATH=$(UBOOT_PATH) CROSS_COMPILE=$(CROSS_COMPILER) \
		make ARCH=$(ARCH) all
	@make --no-print-directory $(UBOOT_T)-bootscr
	@if [ -f $(UBOOT_SRCDIR)/u-boot.bin ]; then \
		$(MSG6) "Built bootloader:" $(EMSG); \
	else \
		$(MSG3) "Bootloader build completed but can't find image:" $(EMSG); \
	fi
	@ls -l $(UBOOT_SRCDIR)/u-boot.bin
	@ls -l $(UBOOT_SRCDIR)/boot.scr
	@touch .$(subst .,,$@)

$(UBOOT_T)-meld:
	@meld $(UBOOT_SRCDIR)/include/configs/$(UBOOT_CONFIG) $(DIR_UBOOT)/$(UBOOT_CONFIG_SRC)

$(UBOOT_T)-diff:
	diff -u $(UBOOT_SRCDIR)/include/configs/$(UBOOT_CONFIG) $(DIR_UBOOT)/$(UBOOT_CONFIG_SRC)

$(UBOOT_T)-saveconfig:
	@$(MSG) "================================================================"
	@$(MSG2) "Saving bootloader configuration" $(EMSG)
	@$(MSG) "================================================================"
	@cp $(UBOOT_SRCDIR)/include/configs/$(UBOOT_CONFIG) $(DIR_UBOOT)/$(UBOOT_CONFIG_SRC)

# Create u-boot script images that get stored on the SD card.
$(UBOOT_T)-bootscr: 
	@$(MSG3) "Building $(UBOOT_SCRIPT) into $(UBOOT_SRCDIR)/boot.scr" $(EMSG)
	@if [ ! -f $(UBOOT_SRCDIR)/tools/mkimage ]; then \
		make --no-print-directory $(UBOOT_T)-mkimage; \
	fi
	@$(MKIMAGE) -T script -A $(ARCH) -C none -a 0 -e 0 -n 'BeagleBox u-boot script' \
		-d $(UBOOT_SCRIPT) $(UBOOT_SRCDIR)/boot.scr

$(UBOOT_T)-mkimage: .$(UBOOT_T)-init 
	@make $(UBOOT_T)-verify
	@$(MSG) "================================================================"
	@$(MSG2) "Building mkimage (u-boot)" $(EMSG)
	@$(MSG) "================================================================"
	@echo "make --no-print-directory $(UBOOT_T)-preconfig"
	cd $(UBOOT_SRCDIR) && PATH=$(UBOOT_PATH) CROSS_COMPILE=$(CROSS_COMPILER) \
		make ARCH=$(ARCH) omap3_beagle_config
	@cd $(UBOOT_SRCDIR) && PATH=$(UBOOT_PATH) CROSS_COMPILE=$(CROSS_COMPILER) \
		make ARCH=$(ARCH) tools
	@ls -l $(UBOOT_SRCDIR)/tools/mkimage

$(UBOOT_T)-files: .$(UBOOT_T)
	@$(MSG) "================================================================"
	@$(MSG2) "$(UBOOT_T) artifacts" $(EMSG)
	@$(MSG) "================================================================"
	@ls -l $(UBOOT_SRCDIR)/u-boot.bin
	@ls -l $(UBOOT_SRCDIR)/boot.scr

$(UBOOT_T)-pkg: 
	@$(MSG3) "Gathering for $(UBOOT_T)" $(EMSG)
	@if [ "x$(PKGDIR)" = "x" ]; then \
		$(MSG11) "$(UBOOT_T): PKGDIR is not set.  Skipping package creation." $(EMSG); \
		exit 1; \
	fi
	@if [ -f $(UBOOT_SRCDIR)/u-boot.bin ]; then \
		cp $(UBOOT_SRCDIR)/u-boot.bin $(PKGDIR); \
	else \
		$(MSG11) "Missing u-boot image: $(UBOOT_SRCDIR)/u-boot.bin" $(EMSG); \
	fi
	@if [ -f $(UBOOT_SRCDIR)/boot.scr ]; then \
		cp $(UBOOT_SRCDIR)/boot.scr $(PKGDIR); \
	else \
		$(MSG11) "Missing boot script image: $(UBOOT_SRCDIR)/boot.scr" $(EMSG); \
	fi
	@if [ -f $(DIR_UBOOT)/bootscripts/uEnv.txt ]; then \
		cp $(DIR_UBOOT)/bootscripts/uEnv.txt $(PKGDIR); \
	else \
		$(MSG11) "Missing uEnv.txt script" $(EMSG); \
	fi

$(UBOOT_T)-clean: 
	@if [ -d $(UBOOT_SRCDIR) ]; then cd $(UBOOT_SRCDIR) && make clean; fi
	@rm -f .$(UBOOT_T)

$(UBOOT_T)-clobber:
	@if [ -d $(UBOOT_SRCDIR) ]; then rm -rf $(UBOOT_SRCDIR); fi
	@make --no-print-directory -i $(UBOOT_T)-clean
	@rm -f .$(UBOOT_T)-init .$(UBOOT_T)-patch .$(UBOOT_T)-unpack-patches \
		.$(UBOOT_T)-unpack .$(UBOOT_T)-get .$(UBOOT_T)-get-patches

