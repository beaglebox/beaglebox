# ---------------------------------------------------------------
# Build the root filesystem via Busybox
# ---------------------------------------------------------------
.$(BUSYBOX_T)-get $(BUSYBOX_T)-get:
	@mkdir -p $(BLDDIR) $(BUSYBOX_ARCDIR)
	@if [ ! -f $(BUSYBOX_ARCDIR)/$(BUSYBOX_VERSION).tar.bz2 ]; then \
		$(MSG) "================================================================"; \
		$(MSG3) Retrieving Busybox package $(EMSG); \
		$(MSG) "================================================================"; \
		D=$(BUSYBOX_ARCDIR) S=$(BUSYBOX_PKG_NAME) U=$(BUSYBOX_URL) make --no-print-directory getsw-only; \
	else \
		$(MSG3) Busybox package is cached $(EMSG); \
	fi
	@touch .$(subst .,,$@)

.$(BUSYBOX_T)-unpack $(BUSYBOX_T)-unpack: .$(BUSYBOX_T)-get
	@$(MSG3) "Unpacking Busybox" $(EMSG)
	@mkdir -p $(BUSYBOX_BLDDIR) $(BUSYBOX_ARCDIR)
	@if [ ! -d $(BUSYBOX_SRCDIR) ]; then \
		tar -C $(BLDDIR) -$(BUSYBOX_JZ)xf $(BUSYBOX_ARCDIR)/$(BUSYBOX_PKG_NAME); \
	fi
	@touch .$(subst .,,$@)

.$(BUSYBOX_T)-patch $(BUSYBOX_T)-patch: .$(BUSYBOX_T)-unpack
	@if [ -d $(BUSYBOX_PATCHDIR) ]; then \
		for patchname in `ls -1 $(BUSYBOX_PATCHDIR)`; do \
			$(MSG3) Applying $$patchname $(EMSG); \
			cd $(BUSYBOX_SRCDIR) && patch -Np1 -r - < $(BUSYBOX_PATCHDIR)/$$patchname 2>&1 >/dev/null ; \
		done; \
	fi
	@touch .$(subst .,,$@)

# Setup for the root filesystem build
.$(BUSYBOX_T)-init $(BUSYBOX_T)-init: .$(BUSYBOX_T)-patch 
	@touch .$(subst .,,$@)

$(BUSYBOX_T)-preconfig: 
	@if [ -f $(BUSYBOX_CONFIG) ]; then \
		$(MSG3) "Using local config" $(EMSG); \
		cp $(BUSYBOX_CONFIG) $(BUSYBOX_SRCDIR)/.config; \
		cd $(BUSYBOX_SRCDIR) && PATH=$(BUSYBOX_PATH) \
			make -j$(JOBS) ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILER) oldconfig; \
	else \
		$(MSG6) "No local config - using defconfig" $(EMSG); \
		cd $(BUSYBOX_SRCDIR) && PATH=$(BUSYBOX_PATH) \
			make -j$(JOBS) ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILER) $(BUSYBOX_DEFCONFIG); \
	fi

$(BUSYBOX_T)-config: 
	@cp $(BUSYBOX_CONFIG) $(BUSYBOX_SRCDIR)/.config
	@sed -i 's%\[BLDDIR\]%$(BUSYBOX_BLDDIR)%g' $(BUSYBOX_SRCDIR)/.config
	@sed -i 's%\[CTNG_SAMPLE\]%$(CTNG_SAMPLE)%g' $(BUSYBOX_SRCDIR)/.config

.$(BUSYBOX_T) $(BUSYBOX_T): .$(XCC_T) .$(BUSYBOX_T)-init
	@$(MSG) "================================================================"
	@$(MSG2) "Building BUSYBOX" $(EMSG)
	@$(MSG) "================================================================"
	@mkdir -p $(BUSYBOX_BLDDIR)/images $(BUSYBOX_ARCDIR)
	@make $(BUSYBOX_T)-config
	@cd $(BUSYBOX_SRCDIR) && make oldconfig
	@make $(BUSYBOX_T)-preconfig
	@$(MSG3) "Running BUSYBOX build" $(EMSG)
	@cd $(BUSYBOX_SRCDIR) && PATH=$(BUSYBOX_PATH) make CROSS_COMPILE=$(CROSS_COMPILER)
	@cd $(BUSYBOX_SRCDIR) && PATH=$(BUSYBOX_PATH) make CROSS_COMPILE=$(CROSS_COMPILER) install
	@if [ -s $(BUSYBOX_BLDDIR)/busybox/linuxrc ]; then \
		$(MSG6) "Built root filesystem:" $(EMSG); \
	else \
		$(MSG3) "Busybox completed but can't find root filesystem:" $(EMSG); \
	fi
	@ls -l $(BUSYBOX_BLDDIR)/busybox
	@touch .$(subst .,,$@)

$(BUSYBOX_T)-menuconfig: $(BUSYBOX_T)-config
	@cd $(BUSYBOX_SRCDIR) && make BUSYBOX_CONFIG=$(BUSYBOX_SRCDIR)/.busybox_config menuconfig

$(BUSYBOX_T)-meld:
	@meld $(BUSYBOX_SRCDIR)/.config $(BUSYBOX_CONFIG)

$(BUSYBOX_T)-saveconfig:
	@$(MSG) "================================================================"
	@$(MSG2) "Saving Busybox configuration" $(EMSG)
	@$(MSG) "================================================================"
	@cp $(BUSYBOX_SRCDIR)/.config $(BUSYBOX_CONFIG) 
	@sed -i 's%$(BUSYBOX_BLDDIR)%\[BLDDIR\]%g' $(BUSYBOX_CONFIG)
	@sed -i 's%$(CTNG_SAMPLE)%\[CTNG_SAMPLE\]%g' $(BUSYBOX_CONFIG)

# This is a no-op because we don't use the artifacts from a busybox build.
# These targets are only for testing configurations of busybox for use with buildroot.
$(BUSYBOX_T)-files:

# This is a no-op because we don't use the artifacts from a busybox build.
# These targets are only for testing configurations of busybox for use with buildroot.
$(BUSYBOX_T)-pkg:
	@$(MSG3) "No packages for $(BUSYBOX_T)" $(EMSG)

$(BUSYBOX_T)-clean:
	@if [ -d "$(BUSYBOX_SRCDIR)" ]; then cd $(BUSYBOX_SRCDIR) && make -i clean; fi
	@rm -rf $(BUSYBOX_BLDDIR) 
	@rm -f .$(BUSYBOX_T) 

$(BUSYBOX_T)-clobber: 
	@if [ -d "$(BUSYBOX_SRCDIR)" ]; then rm -rf $(BUSYBOX_SRCDIR); fi
	@make --no-print-directory -i $(BUSYBOX_T)-clean
	@rm -f .$(BUSYBOX_T)-init .$(BUSYBOX_T)-unpack .$(BUSYBOX_T)-get .$(BUSYBOX_T)-patch


