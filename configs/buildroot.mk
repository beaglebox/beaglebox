# ---------------------------------------------------------------
# Build the root filesystem via Buildroot
# ---------------------------------------------------------------
# Verify the existance of required components, if any
$(buildroot)-verify:
	@if [ "x$(XCC_PREFIXDIR)" = "x" ]; then \
		$(MSG11) "Missing XCC_PREFIXDIR" $(EMSG); \
		$(MSG11) "Try setting XI on the command line." $(EMSG); \
		exit 1; \
	fi
	@if [ ! -d $(XCC_PREFIXDIR) ]; then \
		$(MSG11) "No such directory: $(XCC_PREFIXDIR)" $(EMSG); \
		$(MSG11) "Try setting XI on the command line." $(EMSG); \
		exit 1; \
	fi

.$(BUILDROOT_T)-validate $(BUILDROOT_T)-validate:
	@if [ ! -x $(MKIMAGE) ]; then \
		$(MSG3) "Can't find $(MKIMAGE)" $(EMSG); \
		exit 1; \
	fi
	@touch .$(subst .,,$@)

.$(BUILDROOT_T)-get $(BUILDROOT_T)-get: .$(BUILDROOT_T)-validate
	@mkdir -p $(BLDDIR) $(BUILDROOT_ARCDIR)
	@if [ ! -f $(BUILDROOT_ARCDIR)/$(BUILDROOT_VERSION).tar.bz2 ]; then \
		$(MSG) "================================================================"; \
		$(MSG3) Retrieving Buildroot package $(EMSG); \
		$(MSG) "================================================================"; \
		D=$(BUILDROOT_ARCDIR) S=$(BUILDROOT_PKG_NAME) U=$(BUILDROOT_URL) make --no-print-directory getsw-only; \
	else \
		$(MSG3) Buildroot package is cached $(EMSG); \
	fi
	@touch .$(subst .,,$@)

# Unpack the archive into the build directory.  
# If the archive was a snapshot, we need to rename the unpacked directory.
.$(BUILDROOT_T)-unpack $(BUILDROOT_T)-unpack: .$(BUILDROOT_T)-get
	@$(MSG3) "Unpacking Buildroot" $(EMSG)
	@mkdir -p $(BUILDROOT_BLDDIR) $(BUILDROOT_ARCDIR)
	@if [ ! -d $(BUILDROOT_SRCDIR) ]; then \
		tar -C $(BLDDIR) -$(BUILDROOT_JZ)xf $(BUILDROOT_ARCDIR)/$(BUILDROOT_PKG_NAME); \
	fi
	@if [ -d $(BLDDIR)/buildroot ]; then \
		mv $(BLDDIR)/buildroot $(BLDDIR)/$(BUILDROOT_VERSION); \
	fi
	@touch .$(subst .,,$@)

.$(BUILDROOT_T)-patch $(BUILDROOT_T)-patch: .$(BUILDROOT_T)-unpack
	@if [ -d $(DIR_BUILDROOT)/patches/$(BUILDROOT_RELEASE) ]; then \
		for patchname in `ls -1 $(DIR_BUILDROOT)/patches/$(BUILDROOT_RELEASE)/*.patch`; do \
			$(MSG3) Applying $$patchname $(EMSG); \
			cd $(BUILDROOT_SRCDIR) && patch -Np1 -r - < $$patchname; \
		done; \
	fi
	@touch .$(subst .,,$@)

.$(BUILDROOT_T)-package $(BUILDROOT_T)-package: .$(BUILDROOT_T)-patch
	@if [ -d $(DIR_BUILDROOT)/package/$(BUILDROOT_RELEASE) ]; then \
		for pkgname in `ls -1 $(DIR_BUILDROOT)/package/$(BUILDROOT_RELEASE)`; do \
			if [ "$$pkgname" != "CVS" ]; then \
				$(MSG3) Installing $$pkgname $(EMSG); \
				rsync -ar --exclude "CVS/" \
					$(DIR_BUILDROOT)/package/$(BUILDROOT_RELEASE)/$$pkgname $(BUILDROOT_SRCDIR)/package/; \
			fi; \
		done; \
	fi
	@touch .$(subst .,,$@)

# Setup for the root filesystem build
.$(BUILDROOT_T)-init $(BUILDROOT_T)-init: .$(BUILDROOT_T)-package 
	@touch .$(subst .,,$@)

# Preconfiguration happens each time the build is run, right before the compilation
# starts.  Doing it each time allows us to apply our more recent changes to the build tree.
# The process includes:
# 1. Install our buildroot packages
# 2. Install our target skeleton
# 3. Grab kernel modules and firmware
$(BUILDROOT_T)-preconfig:
	@make --no-print-directory $(BUILDROOT_T)-preconfig-customize
	@if [ -d $(KERNEL_SRCDIR)/modules ]; then \
		rsync -avr $(KERNEL_SRCDIR)/modules/* $(BUILDROOT_SRCDIR)/package/customize/source; \
	else \
		$(MSG11) "Can't find kernel modules" $(EMSG); \
	fi
	@if [ -d $(SGX_BLDDIR) ]; then \
		rsync -avr $(SGX_BLDDIR)/* $(BUILDROOT_SRCDIR)/package/customize/source; \
	else \
		$(MSG11) "Can't find SGX modules" $(EMSG); \
	fi

$(BUILDROOT_T)-preconfig-customize:
	@$(MSG3) "Copying in custom target skeleton" $(EMSG)
	@rm -f $(BUILDROOT_SRCDIR)/output/build/.customize
	@rm -rf $(BUILDROOT_SRCDIR)/package/customize/source/*
	@rm -f $(BUILDROOT_SRCDIR)/package/customize/source/.empty
	@rsync -ar --exclude "CVS/" $(BUILDROOT_SKELETON)/* $(BUILDROOT_SRCDIR)/package/customize/source
	@chmod 755 $(BUILDROOT_SRCDIR)/package/customize/source/etc/init.d/*
	@echo "BeagleBox $(BEAGLEBOX_VERSION)" > $(BUILDROOT_SRCDIR)/package/customize/source/etc/beaglebox-version

# Config target copies in our copies of the configuration files for buildroot
# and busybox, updating tags with real values.  This gets run with each
# build so the latest configuration is picked up.  Use *-menuconfig and 
# *-saveconfig to edit and save configuration changes.
$(BUILDROOT_T)-config:
	@cp $(BUILDROOT_CONFIG) $(BUILDROOT_SRCDIR)/.config
	@sed -i 's%\[DLDIR\]%$(BUILDROOT_ARCDIR)%g' $(BUILDROOT_SRCDIR)/.config
	@sed -i 's%\[BLDDIR\]%$(BUILDROOT_BLDDIR)%g' $(BUILDROOT_SRCDIR)/.config
	@sed -i 's%\[XCC_PREFIXDIR\]%$(XCC_PREFIXDIR)%g' $(BUILDROOT_SRCDIR)/.config
	@sed -i 's%\[CTNG_SAMPLE\]%$(CTNG_SAMPLE)%g' $(BUILDROOT_SRCDIR)/.config
	@cp $(BUILDROOT_BUSYBOX_CONFIG) $(BUILDROOT_SRCDIR)/.busybox_config
	@sed -i 's%\[BLDDIR\]%$(BUILDROOT_BLDDIR)%g' $(BUILDROOT_SRCDIR)/.busybox_config

# This makes it possible to rebuild the rootfs by first cleaning out the custom package
# and then rerunning buildroot, which will just rebuild its components with the updated
# custom package.
$(BUILDROOT_T)-rebuild: .$(BUILDROOT_T)-clean-custom
	@$(MSG3) "Forcing rebuild of root filesystem" $(EMSG)
	@rm -f $(BUILDROOT_SRCDIR)/output/build/.root .$(BUILDROOT_T)
	@make --no-print-directory $(BUILDROOT_T)

.$(BUILDROOT_T)-clean-custom:
	@$(MSG3) "Cleaning custom files from filesystem" $(EMSG)
	@cd $(BUILDROOT_SRCDIR)/package/customize/source/ && \
		for file in `find . -type f`; do \
			if [ -e $$file -a -f $$file ]; then \
				rm -f $(BUILDROOT_SRCDIR)/output/target/$$file; \
			fi; \
		done
	@rm -rf $(BUILDROOT_SRCDIR)/package/customize/source/* 

ifeq ($(SGX),1)
$(BUILDROOT_T): .$(SGX_T) .$(BUILDROOT_T)
else
$(BUILDROOT_T): .$(BUILDROOT_T)
endif

.$(BUILDROOT_T): .$(BUILDROOT_T)-init
	@make -s --no-print-directory $(buildroot)-verify 
	@$(MSG) "================================================================"
	@$(MSG2) "Building $(BUILDROOT_T)" $(EMSG)
ifeq ($(SGX),1)
	@$(MSG2) "with SGX patches." $(EMSG)
endif
	@$(MSG) "================================================================"
	@mkdir -p $(BUILDROOT_BLDDIR)/images $(BUILDROOT_ARCDIR)
	@make --no-print-directory $(BUILDROOT_T)-preconfig
	@make --no-print-directory $(BUILDROOT_T)-config
	@cd $(BUILDROOT_SRCDIR) && make oldconfig
	@$(MSG3) "Running BUILDROOT build" $(EMSG)
	@cd $(BUILDROOT_SRCDIR) && make BUSYBOX_CONFIG=$(BUILDROOT_SRCDIR)/.busybox_config
	@ls -l $(BUILDROOT_SRCDIR)/output/images/
	@touch .$(subst .,,$@)

$(BUILDROOT_T)-mkimage: 
	@if [ -f $(BUILDROOT_SRCDIR)/output/images/rootfs.ext2 ]; then \
		$(MKIMAGE) -T ramdisk -C bzip2 -n 'BeagleBox Ramdisk Image' \
			-d $(BUILDROOT_SRCDIR)/output/images/rootfs.ext2 $(BUILDROOT_SRCDIR)/output/images/ramdisk.bz2
	else \
		$(MSG3) "Can't create flash image of root filesystem: rootfs.ext2.bz2 is missing" $(EMSG); \
	fi

$(BUILDROOT_T)-menuconfig:
	@cd $(BUILDROOT_SRCDIR) && make BUSYBOX_CONFIG=$(BUILDROOT_SRCDIR)/.busybox_config menuconfig

$(BUILDROOT_T)-meld:
	@meld $(BUILDROOT_SRCDIR)/.config $(BUILDROOT_CONFIG)

$(BUILDROOT_T)-diff:
	@diff -u $(BUILDROOT_SRCDIR)/.config $(BUILDROOT_CONFIG)

# Build an ext3 filesystem from the buildroot package
$(BUILDROOT_T)-ext3:
	dd if=/dev/zero of=$(BUILDROOT_SRCDIR)/output/images/rootfs.ext3 bs=1024 count=921600
	mke2fs -j $(BUILDROOT_SRCDIR)/output/images/rootfs.ext3
	sudo mount -o loop $(BUILDROOT_SRCDIR)/output/images/rootfs.ext3 /mnt
	sudo tar -C /mnt -xvf $(BUILDROOT_SRCDIR)/output/images/rootfs.tar
	sudo umount /mnt

$(BUILDROOT_T)-saveconfig:
	@$(MSG) "================================================================"
	@$(MSG2) "Saving BUILDROOT configuration" $(EMSG)
	@$(MSG) "================================================================"
	@cp $(BUILDROOT_SRCDIR)/.config $(BUILDROOT_CONFIG) 
	@sed -i 's%$(BUILDROOT_ARCDIR)%\[DLDIR\]%g' $(BUILDROOT_CONFIG)
	@sed -i 's%$(BUILDROOT_BLDDIR)%\[BLDDIR\]%g' $(BUILDROOT_CONFIG)
	@sed -i 's%$(XCC_PREFIXDIR)%\[XCC_PREFIXDIR\]%g' $(BUILDROOT_CONFIG)
	@sed -i 's%$(CTNG_SAMPLE)%\[CTNG_SAMPLE\]%g' $(BUILDROOT_CONFIG)

$(BUILDROOT_T)-files: .$(BUILDROOT_T)
	@$(MSG) "================================================================"
	@$(MSG2) "$(BUILDROOT_T) artifacts" $(EMSG)
	@$(MSG) "================================================================"
	@ls -l $(BUILDROOT_SRCDIR)/output/images/

$(BUILDROOT_T)-pkg: 
	@$(MSG3) "Gathering for $(BUILDROOT_T)" $(EMSG)
	@if [ -f $(BUILDROOT_SRCDIR)/output/images/rootfs.ext2 ]; then \
		cp $(BUILDROOT_SRCDIR)/output/images/rootfs.tar $(PKGDIR)/ramdisk.tar; \
	else \
		$(MSG11) "Missing ramdisk image: $(BUILDROOT_SRCDIR)/output/images/rootfs.tar" $(EMSG); \
	fi

$(BUILDROOT_T)-clean:
	@if [ -d "$(BUILDROOT_SRCDIR)" ]; then cd $(BUILDROOT_SRCDIR) && make -i clean; fi
	@rm -rf $(BUILDROOT_BLDDIR) 
	@rm -f .$(BUILDROOT_T) 

$(BUILDROOT_T)-clobber: 
	@if [ -d "$(BUILDROOT_SRCDIR)" ]; then rm -rf $(BUILDROOT_SRCDIR); fi
	@make --no-print-directory -i $(BUILDROOT_T)-clean
	@rm -f .$(BUILDROOT_T)-init .$(BUILDROOT_T)-package .$(BUILDROOT_T)-patch .$(BUILDROOT_T)-unpack \
		.$(BUILDROOT_T)-get .$(BUILDROOT_T)-validate 


