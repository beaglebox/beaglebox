# ---------------------------------------------------------------
# Package everything.
# ---------------------------------------------------------------
$(PKG_T)-init: 
	@mkdir -p $(PKGDIR)

$(PKG_T): $(PKG_T)-init $(PKG_T)-scripts $(PKG_T)-mlo
	@$(MSG) "================================================================"
	@$(MSG2) "Gathering package files" $(EMSG)
	@$(MSG) "================================================================"
	@for component in $(PKG_TARGETS); do \
		if [ "$$component" != "$(PKG_T)" ]; then \
			make --no-print-directory $$component-pkg; \
		fi; \
	done
	@$(MSG) "================================================================"
	@$(MSG2) "Package List" $(EMSG)
	@$(MSG) "================================================================"
	@ls -lR $(PKGDIR)

# ---------------------------------------------------------------
# Component specific packaging 
# ---------------------------------------------------------------
$(PKG_T)-scripts: $(PKG_T)-init
	@$(MSG) "================================================================"
	@$(MSG2) "Grabbing installation scripts" $(EMSG)
	@$(MSG) "================================================================"
	@cp $(SCRIPTDIR)/mk*.sh $(PKGDIR)

$(PKG_T)-mlo: $(PKG_T)-init
	@$(MSG) "================================================================"
	@$(MSG2) "Grabbing MLO binary" $(EMSG)
	@$(MSG) "================================================================"
	@cd $(PKGDIR) && wget http://beagleboard.googlecode.com/files/MLO_revc_v3
	@cd $(PKGDIR) && mv MLO_revc_v3 MLO

$(PKG_T)-clean:
	@rm -rf $(PKGDIR) 

$(PKG_T)-clobber: $(PKG_T)-clean

