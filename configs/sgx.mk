# ---------------------------------------------------------------
# Package the SGX libraries and support files for inclusion
# in the root file system.
# ---------------------------------------------------------------

# ---------------------------------------------------------------
# Allow manual retrieval and unpacking of currently configured version
# ---------------------------------------------------------------
$(SGX_T)-get:
	@mkdir -p $(SGX_ARCDIR) 
	@if [ ! -f $(SGX_ARCDIR)/$(SGX_PKG_NAME) ]; then \
		$(MSG) "================================================================"; \
		$(MSG3) "Retrieving SGX Version $(SGX_VERSION)" $(EMSG); \
		$(MSG3) "This will probably take a long time to complete." $(EMSG); \
		$(MSG) "================================================================"; \
		D=$(SGX_ARCDIR) S=$(SGX_PKG_NAME) U=$(SGX_URL) make --no-print-directory getsw-only; \
	else \
		$(MSG3) SGX package is cached $(EMSG); \
	fi

ifeq ($(SGX_VPREFIX), 3)
$(SGX_T)-unpack: $(SGX_T)-get
	@if [ ! -d $(SGX_SDKDIR) ]; then \
		$(MSG3) "Unpacking SGX Version $(SGX_VERSION)" $(EMSG); \
		chmod +x $(SGX_ARCDIR)/$(SGX_PKG_NAME); \
		cd $(SGX_ARCDIR) && ./$(SGX_PKG_NAME) --mode silent --prefix $(SGX_SDKDIR); \
	fi
else
$(SGX_T)-unpack: $(SGX_T)-get
	@if [ ! -d $(SGX_SDKDIR) ]; then \
		$(MSG3) "Unpacking SGX Version $(SGX_VERSION)" $(EMSG); \
		chmod +x $(SGX_ARCDIR)/$(SGX_PKG_NAME); \
		cp $(DIR_SGX)/sgx-unpack.sh $(SGX_ARCDIR)/ ; \
		cd $(SGX_ARCDIR) && ./sgx-unpack.sh $(SGX_PKG_NAME) $(SGX_SDKDIR); \
	fi
endif

# ---------------------------------------------------------------
# Verify we can find the SGX SDK package.
# ---------------------------------------------------------------
$(SGX_T)-verify:
	@if [ ! -d $(SGX_SDKDIR) ]; then \
		$(MSG11) "SGX_SDKDIR not found: $(SGX_SDKDIR):" $(EMSG); \
		$(MSG11) "Try: make $(SGX_T)-unpack" $(EMSG); \
		exit -1; \
	fi
	@if [ ! -d $(KERNEL_SRCDIR) ]; then \
		$(MSG11) "KERNEL_SRCDIR not found: $(KERNEL_SRCDIR):" $(EMSG); \
		$(MSG11) "Try: make $(KERNEL_T)" $(EMSG); \
		exit -1; \
	fi
	@$(MSG) "================================================================"
	@$(MSG3) SGX Library Packaging $(EMSG)
	@$(MSG) "================================================================"
	@$(MSG3) "Found SGX SDK" $(EMSG); \

# ---------------------------------------------------------------
# General Installation: 
# ---------------------------------------------------------------

$(SGX_T)-install-mkdir: 
	@$(MSG3) Creating SGX rootfs stub tree $(EMSG)
	@install -d $(SGX_BLDDIR)/lib
	@install -d $(SGX_BLDDIR)/etc/init.d/
	@install -d $(SGX_BLDDIR)/usr/local

# ---------------------------------------------------------------
# Kernel Modules Installation: 
# ---------------------------------------------------------------

.$(SGX_T)-kernel-rules $(SGX_T)-kernel-rules: 
	@make --no-print-directory $(SGX_T)-verify
	@make --no-print-directory $(SGX_T)-install-mkdir
	@sed -i 's%HOME=.*%HOME=$(BLDDIR)%' $(SGX_SRCDIR)/Rules.make
	@sed -i 's%CSTOOL_DIR=.*%CSTOOL_DIR=$(XCC_PREFIXDIR)%' $(SGX_SRCDIR)/Rules.make
	@sed -i 's%CSTOOL_PREFIX=.*%CSTOOL_PREFIX=$(CROSS_COMPILER)%' $(SGX_SRCDIR)/Rules.make
	@sed -i 's%KERNEL_INSTALL_DIR=.*%KERNEL_INSTALL_DIR=$(KERNEL_SRCDIR)%' $(SGX_SRCDIR)/Rules.make
	@sed -i 's%TARGETFS_INSTALL_DIR=.*%TARGETFS_INSTALL_DIR=$(SGX_BLDDIR)%' $(SGX_SRCDIR)/Rules.make
	@sed -i 's%GRAPHICS_INSTALL_DIR=.*%GRAPHICS_INSTALL_DIR=$(SGX_SRCDIR)%' $(SGX_SRCDIR)/Rules.make
	@touch .$(subst .,,$@)

.$(SGX_T)-kernel: .$(SGX_T)-kernel-rules
	@cd $(SGX_SRCDIR) && make ARCH=arm OMAPES=3.x SUPPORT_XORG=1 all
	@cd $(SGX_SRCDIR) && make ARCH=arm OMAPES=3.x SUPPORT_XORG=1 install
	@touch .$(subst .,,$@)

# ---------------------------------------------------------------
# armhf binary library modules Installation (from RNelson): 
# ---------------------------------------------------------------

# ---------------------------------------------------------------
# Front End to SGX build
# ---------------------------------------------------------------
#
.$(SGX_T): .$(SGX_T)-kernel

$(SGX_T): .$(SGX_T)

# SGX file list and packaging are no-ops. The artifacts are placed in their appropriate places
# by the install targets, called elsewhere.
$(SGX_T)-files: 
	@$(MSG3) "See $(SGX_BLDDIR)" $(EMSG)

$(SGX_T)-pkg: 
	@$(MSG3) "See $(SGX_BLDDIR)" $(EMSG)

# ---------------------------------------------------------------
# Clean up from build
# ---------------------------------------------------------------
$(SGX_T)-clean: $(SGX_T)-clean-kernel
	@ if [ -d "$(SGX_BLDDIR)" ]; then rm -rf $(SGX_BLDDIR); fi
	@ rm -f .$(SGX_T) .$(SGX_T)-kernel .$(SGX_T)-kernel-rules .$(SGX_T)-configure

$(SGX_T)-clean-kernel: 
	@ cd $(SGX_SDKDIR) ] && make -i clean

$(SGX_T)-clobber: 
	@ rm -rf $(SGX_SDKDIR)
	@ make --no-print-directory -i $(SGX_T)-clean

