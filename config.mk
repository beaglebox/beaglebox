#----------------------------------------------------------------------
# Common Config variables and targets.
# Includes configurations from configs directory.
#----------------------------------------------------------------------

# Specify a SourceForge mirror
# You can override this by specifying it in your environment.
ifndef SF_MIRROR
SF_MIRROR = voxel
endif

#---------------------------------------------------------------------
# Project Architecture and Vendor
ARCH := arm
HW   = beagleboard

# Beagle Box Version ID
BEAGLEBOX_VERSION := 0.8.0

# Number of parallel jobs.  Override this on the command line.
JOBS = 4

# Who am I?
UID = $(shell id -u)

#---------------------------------------------------------------------
# Target names - these are also used with a "." prefix as
# empty target files for various build sections.
# TARGETS gets updated in each component's .cfg file.

INIT_T          := init
TARGETS         = 
PKG_TARGETS     = 

#---------------------------------------------------------------------
# Directories: 
# The build and archive downloads are kept in parallel directories from
# the source tree so hg status won't get confused by all the new files.
TOPDIR              := $(shell pwd)
SRCDIR              := $(TOPDIR)/src
ARCDIR              := $(TOPDIR)/../archive
BLDDIR              := $(TOPDIR)/../bld
PKGDIR              := $(TOPDIR)/../pkg
SCRIPTDIR           := $(TOPDIR)/scripts

#---------------------------------------------------------------------
# Include the configs directory files after the common configs
# Note: Order here is important

include configs/xcc.cfg
include configs/uboot.cfg
include configs/kernel.cfg
include configs/busybox.cfg
include configs/sgx.cfg
include configs/buildroot.cfg
include configs/pkg.cfg
include configs/opkg.cfg

#---------------------------------------------------------------------
# Include the component makefiles
# Note: Order here is important
include configs/xcc.mk
include configs/uboot.mk
include configs/kernel.mk
include configs/busybox.mk
include configs/sgx.mk
include configs/buildroot.mk
include configs/pkg.mk
include configs/opkg.mk

#---------------------------------------------------------------------
# Config display target
showconfig:
	@$(MSG3) Common Configuration $(EMSG)
	@echo "Components           :$(TARGETS)"
	@echo "Packaged Components  :$(PKG_TARGETS)"
	@echo "ARCH                 : $(ARCH)"
	@echo "HW                   : $(HW)"
	@echo "SRCDIR               : $(SRCDIR)"
	@echo "ARCDIR               : $(ARCDIR)"
	@echo "BLDDIR               : $(BLDDIR)"
	@echo "CROSS_COMPILER       : $(CROSS_COMPILER)"
	@echo "XCC_PREFIXDIR        : $(XCC_PREFIXDIR)"
	@echo "XCC_PREFIX           : $(XCC_PREFIX)"
	@echo "U-Boot config (USRC) : uboot-$(USRC)"
	@echo "Kernel config (KSRC) : kernel-$(KSRC)"

