#!/bin/bash -p
# 
# Copy the appropriate files, in the correct order,
# to the boot partition on the SD card.
# Then, if requested, copy the rootfs to its partition.
#
# Order of install:
# MLO as MLO - X-Loader
# u-boot.bin - Bootloader (Das U-Boot)
# uImage.bin - Linux Kernel
# boot.scr - Compiled boot script
# uEnv.txt - Generic loader script for boot.scr
# ramdisk - Root file system
# ------------------------------------------------

#--------------------------------------------------------------
# Initialization
#--------------------------------------------------------------
BOOTDIR=none
ROOTFSDIR=none
SDROOTFS=/media/bbrootfs

MLO="MLO"
UBOOT="u-boot.bin"
UIMAGE="uImage.bin"
BOOTSCR="boot.scr"
UENV="uEnv.txt"
RAMDISK="ramdisk.tar"

#--------------------------------------------------------------
# Provide command line usage assistance
#--------------------------------------------------------------
function doHelp
{
    echo ""
    echo "$0 [-b <dir> | -d <device> | -u <file> | -l <file> | -s <file> | -t <file>] | -r <file> "
    echo "where"
    echo "-b <dir>     Directory where boot partition is mounted; Default: $BOOTDIR"
    echo "-d <device>  Device (such as /dev/sdb2) for root file system; Default: $ROOTFSDIR"
    echo "-u <file>    Image file to use for bootloader; Default: $UBOOT"
    echo "-l <file>    Image file to use for Linux kernel; Default: $UIMAGE"
    echo "-s <file>    Boot script file; Default: $BOOTSCR"
    echo "-t <file>    Text boot script; Default: $UENV"
    echo "-r <file>    Ramdisk image for root file system; Default: $RAMDISK"
    echo ""
    echo "If -b is specified then the boot files will be installed"
    echo "If -d is specified then the ramdisk will be installed"
    echo "Default is to do nothing."
}

#--------------------------------------------------------------
# Read command line arguments
#--------------------------------------------------------------
while getopts ":b:r:u:l:s:t:d:" Option
do
    case $Option in
    b) BOOTDIR=$OPTARG;;
    d) ROOTFSDIR=$OPTARG;;
    u) UBOOT=$OPTARG;;
    l) UIMAGE=$OPTARG;;
    s) BOOTSCR=$OPTARG;;
    t) UENV=$OPTARG;;
    r) RAMDISK=$OPTARG;;
    *) doHelp; exit 0;;
    esac
done


#--------------------------------------------------------------
# Error checking
#--------------------------------------------------------------
if [ "$BOOTDIR" != "none" ]
then
    if [ ! -d $BOOTDIR ]; then
        echo "The boot directory ($BOOTDIR) is not mounted.  Installation aborted."
        exit 1
    fi
    if [ ! -f $MLO ]; then
        echo "Your missing the MLO binary.  Install aborted."
        exit 1
    fi
    if [ ! -f $UBOOT ]; then
        echo "Your missing the bootloader binary.  Install aborted."
        exit 1
    fi
    if [ ! -f $UIMAGE ]; then
        echo "Your missing the Linux kernel binary.  Install aborted."
        exit 1
    fi
    if [ ! -f $UENV ]; then
        echo "Your missing the uEnv.txt script.  Install aborted."
        exit 1
    fi
    if [ ! -f $BOOT ]; then
        echo "Your missing the boot.scr boot script.  Install aborted."
        exit 1
    fi
else
	echo "No boot directory specified. Boot files will not be installed."
fi

if [ "$ROOTFSDIR" != "none" ]
then
    if [ ! -b $ROOTFSDIR ]; then
        echo "The rootfs device ($ROOTFSDIR) is not available.  Installation aborted."
        exit 1
    fi
    if [ ! -f $RAMDISK ]; then
        echo "Your missing the rootfs file ($RAMDISK).  Install aborted."
        exit 1
    fi
else
	echo "No root file system device specified. Ramdisk will not be installed."
fi

#--------------------------------------------------------------
# Install boot partition files
#--------------------------------------------------------------
if [ "$BOOTDIR" != "none" ]
then
    sudo cp $MLO $BOOTDIR
    sudo cp $UBOOT $BOOTDIR
    sudo cp $UIMAGE $BOOTDIR
    sudo cp $UENV $BOOTDIR
    sudo cp $BOOTSCR $BOOTDIR
fi

#--------------------------------------------------------------
# Install ramdisk image
#--------------------------------------------------------------
if [ "$ROOTFSDIR" != "none" ]
then
    sudo mkdir -p $SDROOTFS
    sudo mount $ROOTFSDIR $SDROOTFS
    sudo tar -C $SDROOTFS -xvf $RAMDISK 
    sudo sync
    sudo umount $ROOTFSDIR $SDROOTFS
    sudo rmdir $SDROOTFS
fi

