#!/usr/bin/perl -w
# Strip the patches list from the 
# BB Linux kernel file and output to
# standard out.
# Currently works with Linux-omap-2.6.32.
# Not clear if it will work with others.
# ----------------------------------

if ( ! -f $ARGV[0] ) { die "No such file: $ARGV[0]"; }
$filename = $ARGV[0];

$keep = 0;
open(FD, "$filename");
while(<FD>)
{
	next if (/^"/ );
	next if (/^\n/ );
	next if (/beaglebug-full.patch/ );
	if (/^SRC_URI_append =/ ) { $keep = 1; next; }
	if (/^SRC_URI_append_/ && $keep == 1) { $keep = 0; next; }
	if ( $keep == 1 ) { 
		$line = $_; 
		$line =~ s%^.*file://%%;
		$line =~ s% \\%%;
		print $line;
	}
}
close(FD);
