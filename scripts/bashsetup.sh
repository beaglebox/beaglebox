#!/bin/bash -p
# Functions and aliases used to nagivate project directories.
# Include by the ~/bin/cdtools script.
#############################################################################

# -------------------------------------------------------------------
# BeagleBox
# -------------------------------------------------------------------
function beaglebox {
    # Project ID
    PRJ=beaglebox

    # If you will be contributing to BeagleBox, you need to have a CVS user
    # Contact mjhammel@graphics-muse.org to access.
    export CVSROOT=<user>@beaglebox.cvs.sourceforge.net:/cvsroot/beaglebox
    export CVS_RSH=ssh

    # Top of the source tree
    SRCTOP=$HOME/src/ximba

    if [ ! -d $SRCTOP ]
    then
        mkdir -p $SRCTOP
    fi

    # Where I do my dev work
    GM_WORK=$SRCTOP/work

    if [ ! -d $GM_WORK ]
    then
        mkdir -p $GM_WORK
    fi

    # Where the CVS is located
    GM_HOME=$SRCTOP/$PRJ

    if [ ! -d $GM_PROJ ]
    then
        mkdir -p $GM_PROJ
    fi

    # Where the source and build directories live
    # These get created when you check out code (GM_SRC) or do builds.
    GM_SRC=$GM_HOME/src
    GM_BUILD=$GM_HOME/bld
    GM_ARCHIVE=$GM_HOME/archive
    GM_PKG=$GM_HOME/pkg

    # If you are using an cross toolchain outside of the build tree, 
    # set it here.  This is typically used when the toolchain was 
    # created as an RPM and then installed so you don't have to 
    # build it repeatedly.
    # XCC_DIR=<path to top of cross toolchain installation>

    # Where is OpenEmbedded installed
    # Not currently required for BeagleBox, but may be later
    GM_OE=$SRCTOP/angstrom/src/openembedded

    # Where is the AM35x/OMAP35x SGX SDK Installed
    GM_SGX_DIR=$GM_HOME/extras/sdk

    # Make the configured environment available to the build system.
    export GM_WORK
    export GM_ARCHIVE
    export GM_HOME
    export GM_SRC
    export GM_BUILD
    export GM_OE
    export GM_SGX_DIR

    # Some aliases to bounce around directories easily
    alias cdt='cd $SRCTOP'
    alias cdh='cd $GM_HOME'
    alias cdw='cd $GM_WORK'
    alias cdx='cd $GM_SRC'
    alias cdb='cd $GM_BUILD'
    alias cda='cd $GM_ARCHIVE'
    alias cdp='cd $GM_PKG'

    # CVS tree is accessed as "home" or "cvs"
    alias cdh='cd $GM_HOME'
    alias cdcvs='cd $GM_HOME'

    # Show the aliases for this configuration
    alias cd?='cdbb?'
    alias cvsu='cvs update 2>&1 | egrep "^[UPAMRC\?]"'
}
function cdbb? {
echo "
BeagleBox Alias settings:
-----------------------------------------------------------------------------
cdt    cd SRCTOP ($SRCTOP)
cdh    cd GM_HOME ($GM_HOME)
cdw    cd GM_WORK ($GM_WORK)
cdx    cd GM_SRC ($GM_SRC)
cdb    cd GM_BUILD ($GM_BUILD)
cda    cd GM_ARCHIVE ($GM_ARCHIVE)
cdp    cd GM_PKG ($GM_PKG)

GM_PSP_DIR=$GM_PSP_DIR
GM_SGX_DIR=$GM_SGX_DIR

CVSROOT = $CVSROOT
"
}
