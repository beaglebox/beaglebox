#!/bin/bash -p
# Format an SD card for use with BeagleBoard
# Borrowed from Robert Nelson's instructions
# http://elinux.org/BeagleBoardUbuntu#Partition_SD_Card
# --------------------------------------------

if [ "$1" = "" ]
then
	echo "You must supply the SD device on the command line."
	exit 1
fi

DEV=$1
if [ ! -b $DEV ]
then
	echo "$DEV does not exist or is not a block device."
	exit 1
fi

# blank the MMC card
sudo parted -s $DEV mklabel msdos
if [ $? -ne 0 ]
then
	echo "Failed to make label on first partition."
	exit 1
fi

# Create partitions
# First partition will be a small VFAT16 partition
sudo fdisk $DEV << __EOF__
n
p
1

+64M
t
e
p
w
__EOF__

# Second partition will take up the rest of the SD card
# and will be an ext3 partition.
sudo fdisk $DEV << __EOF__
n
p
2


t
2
83
w
__EOF__

# set the partition boot flag
sudo parted --script $DEV set 1 boot on

MMC=`echo $DEV | grep mmc`
echo "MMC=$MMC"

# Format the first partition
if [ "$MMC" = "" ]
then
	sudo mkfs.vfat -F 16 ${DEV}1 -n boot
else
	sudo mkfs.vfat -F 16 ${DEV}p1 -n boot
fi

# Format rootfs partition
if [ "$MMC" = "" ]
then
	sudo mkfs.ext3 -L rootfs ${DEV}2
else
	sudo mkfs.ext3 -L rootfs ${DEV}p2 
fi
