# Example commands that will boot the kernel with omap settings
# Run these manually from the u-boot command prompt
# -------------------------------------------------------------
setenv mmcrootfstype ext2 rootwait
setenv defaultdisplay tv
setenv ramaddr 0x83600000
setenv mmcargs setenv bootargs console=${console} vram=${vram} omapfb.mode=tv:ntsc omapfb.debug=y omapdss.def_disp=${defaultdisplay} root=/dev/ram0 rw initrd=${ramaddr},32M 
run mmcargs
mmc init
printenv
fatload mmc 0 ${loadaddr} uImage.bin
fatload mmc 0 ${ramaddr} ramdisk.gz
bootm ${loadaddr}

# -------------------------------------------------------------
# Run these after logging in to verify the video settings
# -------------------------------------------------------------
cat /proc/cmdline
cat /sys/devices/platform/omapfb/displays |grep tv
