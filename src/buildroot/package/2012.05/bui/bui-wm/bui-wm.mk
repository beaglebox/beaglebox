#############################################################
#
# BUI WM
#
#############################################################

BUI_WM_VERSION 		= master
BUI_WM_SITE 		= git://gitorious.org/bui/bui-wm.git
BUI_WM_AUTORECONF   = YES
BUI_WM_DEPENDENCIES = bui-lib
BUI_WM_CONF_OPT 	= --enable-expat

# Workaround bug in configure script
BUI_WM_CONF_ENV = expat=yes

BUI_WM_POST_EXTRACT_HOOKS   += BUI_WM_DO_AUTOGEN

#############################################################

ifeq ($(BR2_PACKAGE_X11R7_LIBXCOMPOSITE),y)
ifeq ($(BR2_PACKAGE_X11R7_LIBXPM),y)
  BUI_WM_CONF_OPT+=--enable-composite
  BUI_WM_DEPENDENCIES+=xlib_libXcomposite
  BUI_WM_DEPENDENCIES+=xlib_libXpm
endif
endif

#############################################################

$(eval $(call AUTOTARGETS,package/bui,bui-wm))
