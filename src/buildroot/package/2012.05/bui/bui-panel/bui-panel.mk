#############################################################
#
# BUI Panel
#
#############################################################

BUI_PANEL_VERSION 		= master
BUI_PANEL_SITE 			= git://gitorious.org/bui/bui-panel.git
BUI_PANEL_AUTORECONF    = YES
BUI_PANEL_DEPENDENCIES 	= bui-lib
BUI_PANEL_CONF_OPT 		= --enable-expat

BUI_PANEL_POST_EXTRACT_HOOKS   += BUI_PANEL_DO_AUTOGEN

#############################################################

$(eval $(call AUTOTARGETS,package/bui,bui-panel))
