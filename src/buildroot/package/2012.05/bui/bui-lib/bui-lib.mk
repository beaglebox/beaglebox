#############################################################
#
# bui_lib
#
#############################################################

BUI_LIB_VERSION 		= master
BUI_LIB_SITE 			= git://gitorious.org/bui/bui-lib.git
BUI_LIB_AUTORECONF      = YES
BUI_LIB_DEPENDENCIES 	= host-pkg-config expat xlib_libXext 
BUI_LIB_INSTALL_STAGING	= YES
BUI_LIB_INSTALL_TARGET	= YES
BUI_LIB_LIBTOOL_PATCH   = NO
BUI_LIB_CONF_OPT		= --disable-doxygen-docs

BUI_LIB_POST_EXTRACT_HOOKS   += BUI_LIB_DO_AUTOGEN

ifeq ($(BR2_PACKAGE_JPEG),y)
  BUI_LIB_CONF_OPT+=--enable-jpeg
  BUI_LIB_DEPENDENCIES+=jpeg
else
  BUI_LIB_CONF_OPT+=--disable-jpeg
endif

ifeq ($(BR2_PACKAGE_LIBPNG),y)
  BUI_LIB_CONF_OPT+=--enable-png
  BUI_LIB_DEPENDENCIES+=libpng
else
  BUI_LIB_CONF_OPT+=--disable-png
endif

ifeq ($(BR2_PACKAGE_PANGO),y)
  BUI_LIB_CONF_OPT+=--enable-pango
  BUI_LIB_DEPENDENCIES+=pango
else
  BUI_LIB_CONF_OPT+=--disable-pango
endif

$(eval $(call AUTOTARGETS,package/bui,bui-lib))
