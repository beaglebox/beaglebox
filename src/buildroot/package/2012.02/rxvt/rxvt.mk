#############################################################
#
# rxvt
#
#############################################################

RXVT_VERSION      	    = 2.7.10
RXVT_SOURCE       	    = rxvt-$(RXVT_VERSION).tar.gz
RXVT_SITE         	    = http://$(BR2_SOURCEFORGE_MIRROR).dl.sourceforge.net/sourceforge/rxvt/
RXVT_DEPENDENCIES 	    = xlib_libX11 libxcb xlib_libXau
RXVT_INSTALL_TARGET	    = YES
RXVT_INSTALL_TARGET_OPT = DESTDIR=$(TARGET_DIR) install

$(eval $(call AUTOTARGETS,package,rxvt))
