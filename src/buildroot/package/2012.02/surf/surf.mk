#############################################################
#
# surf
#
#############################################################
# UGLY:
# Hardcoded paths need to use pkgconfig, but calling that
# leaves us with target-specifc (not build specific) paths.
# Also, we need a way to identify versions of GTK+, GLib, etc.

SURF_VERSION:=0.4.1
SURF_SOURCE:=surf-$(SURF_VERSION).tar.gz
SURF_SITE:=http://dl.suckless.org/surf/
SURF_DEPENDENCIES = libgtk2 webkit

# Specify the graphics target for GTK+ and headers and libraries required by surf
SURF_TARGET = $(shell grep "^target" $(STAGING_DIR)/usr/lib/pkgconfig/gtk+-2.0.pc|cut -f2 -d"=")

SURF_STG_USRLIB = $(STAGING_DIR)usr/lib
SURF_XLIBS = -lxslt -lXt -lSM -lICE -lXcomposite -lXdamage -lXfixes -lXext -lXrender -lXinerama -lXrandr -lXcursor -lpixman-1 -lxcb -lXau -lXdmcp -lX11
SURF_WKLIBS = -lenchant 
SURF_OLIBS = -lpng -ljpeg -lz -lxml2 -lsqlite3 -licui18n -licuuc -licudata -lexpat
SURF_BLDLIBS = $(SURF_WKLIBS) -lwebkit-1.0 -lgtk-$(SURF_TARGET)-2.0 -lsoup-2.4 -lgdk-$(SURF_TARGET)-2.0 -latk-1.0 -lpangoft2-1.0 -lgdk_pixbuf-2.0 -lpangocairo-1.0 -lcairo -lpango-1.0 -lfreetype -lfontconfig -lgio-2.0 -lgobject-2.0 -lgmodule-2.0 -lgthread-2.0 $(SURF_XLIBS) $(SURF_OLIBS) -lglib-2.0 

# Modify the config.mk file in the source for proper 
# compilation and installation.
define SURF_CONFIGURE_CMDS
	cd $(@D) && \
		sed -i 's%^PREFIX =.*%PREFIX = /usr%' config.mk && \
		sed -i 's%^CC =.*%CC = $(TARGET_CC)%' config.mk && \
 		sed -i 's%^INCS =.*%INCS = -I. -I$(STAGING_DIR)usr/include $$\{GTKINC}%' config.mk && \
		sed -i 's%^LIBS =.*%LIBS = $(TARGET_LDFLAGS) $(SURF_BLDLIBS)%' config.mk 
endef

define SURF_BUILD_CMDS
	PKG_CONFIG_SYSROOT_DIR="$(STAGING_DIR)" \
		PKG_CONFIG="$(PKG_CONFIG_HOST_BINARY)" \
		PKG_CONFIG_PATH="$(STAGING_DIR)/usr/lib/pkgconfig:$(PKG_CONFIG_PATH)" \
 		$(MAKE) CC=$(TARGET_CC) LD=$(TARGET_LD) -C $(@D) all
endef

define SURF_INSTALL_TARGET_CMDS
	PKG_CONFIG_SYSROOT_DIR="$(STAGING_DIR)" \
		PKG_CONFIG="$(PKG_CONFIG_HOST_BINARY)" \
		PKG_CONFIG_PATH="$(STAGING_DIR)/usr/lib/pkgconfig:$(PKG_CONFIG_PATH)" \
		$(MAKE) CC=$(TARGET_CC) LD=$(TARGET_LD) \
			DESTDIR=$(TARGET_DIR) \
			-C $(@D) install
endef

define SURF_CLEAN_CMDS
	$(MAKE) CC=$(TARGET_CC) LD=$(TARGET_LD) -C $(@D) clean
endef

define SURF_UNINSTALL_CMDS
	$(MAKE) CC=$(TARGET_CC) LD=$(TARGET_LD) \
		DESTDIR=$(TARGET_DIR) \
		-C $(@D) uninstall
endef

$(eval $(call GENTARGETS,package,surf))
