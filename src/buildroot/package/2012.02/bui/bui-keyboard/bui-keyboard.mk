#############################################################
#
# BUI Keyboard
#
#############################################################

BUI_KEYBOARD_VERSION 		= 0.1.0
BUI_KEYBOARD_SOURCE 		= bui-keyboard-$(BUI_KEYBOARD_VERSION).tar.gz
BUI_KEYBOARD_SITE 			= http://$(BR2_SOURCEFORGE_MIRROR).dl.sourceforge.net/sourceforge/beaglebox/
BUI_KEYBOARD_DEPENDENCIES 	= bui-lib bui-fakekey

ifeq ($(BR2_PACKAGE_X11R7_LIBXCOMPOSITE),y)
ifeq ($(BR2_PACKAGE_X11R7_LIBXPM),y)
  BUI_KEYBOARD_CONF_OPT+=--enable-composite
  BUI_KEYBOARD_DEPENDENCIES+=xlib_libXcomposite
  BUI_KEYBOARD_DEPENDENCIES+=xlib_libXpm
endif
endif

# Workaround bug in configure script
BUI_KEYBOARD_CONF_ENV = expat=yes

define BUI_KEYBOARD_DO_AUTOGEN
cd $(BUI_KEYBOARD_DIR) && ./autogen.sh
endef

define BUI_KEYBOARD_POST_INSTALL_FIXES
 cp -dpf ./package/bui/bui-keyboard/bui-kbd-wrapper.sh $(TARGET_DIR)/usr/bin/
endef

BUI_KEYBOARD_POST_EXTRACT_HOOKS        += BUI_KEYBOARD_DO_AUTOGEN
BUI_KEYBOARD_POST_INSTALL_TARGET_HOOKS += BUI_KEYBOARD_POST_INSTALL_FIXES

#############################################################

ifeq ($(BR2_PACKAGE_PANGO),y)
  BUI_PKEYBOARD_CONF_OPT+=--enable-pango
else
  BUI_KEYBOARD_DEPENDENCIES+=xlib_libXft
endif

#############################################################

$(eval $(call AUTOTARGETS,package/bui,bui-keyboard))
