#############################################################
#
# BUI Panel
#
#############################################################

BUI_PANEL_VERSION 		= 0.1.1
BUI_PANEL_SOURCE 		= bui-panel-$(BUI_PANEL_VERSION).tar.gz
BUI_PANEL_SITE 			= http://$(BR2_SOURCEFORGE_MIRROR).dl.sourceforge.net/sourceforge/beaglebox/
BUI_PANEL_DEPENDENCIES 	= bui-lib
BUI_PANEL_CONF_OPT 		= --enable-expat

define BUI_PANEL_DO_AUTOGEN
cd $(BUI_PANEL_DIR) && ./autogen.sh
endef
BUI_PANEL_POST_EXTRACT_HOOKS   += BUI_PANEL_DO_AUTOGEN

#############################################################

$(eval $(call AUTOTARGETS,package/bui,bui-panel))
