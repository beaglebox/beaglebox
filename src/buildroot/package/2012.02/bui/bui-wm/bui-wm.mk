#############################################################
#
# BUI WM
#
#############################################################

BUI_WM_VERSION 		= 0.1.0
BUI_WM_SOURCE 		= bui-wm-$(BUI_WM_VERSION).tar.gz
BUI_WM_SITE 		= http://$(BR2_SOURCEFORGE_MIRROR).dl.sourceforge.net/sourceforge/beaglebox/
BUI_WM_DEPENDENCIES = bui-lib
BUI_WM_CONF_OPT 	= --enable-expat

# Workaround bug in configure script
BUI_WM_CONF_ENV = expat=yes

define BUI_WM_DO_AUTOGEN
cd $(BUI_WM_DIR) && ./autogen.sh
endef

BUI_WM_POST_EXTRACT_HOOKS   += BUI_WM_DO_AUTOGEN

#############################################################

ifeq ($(BR2_PACKAGE_X11R7_LIBXCOMPOSITE),y)
ifeq ($(BR2_PACKAGE_X11R7_LIBXPM),y)
  BUI_WM_CONF_OPT+=--enable-composite
  BUI_WM_DEPENDENCIES+=xlib_libXcomposite
  BUI_WM_DEPENDENCIES+=xlib_libXpm
endif
endif

#############################################################

$(eval $(call AUTOTARGETS,package/bui,bui-wm))
