#############################################################
#
# BUI Fakekey
#
#############################################################

BUI_FAKEKEY_VERSION 		= 0.1.0
BUI_FAKEKEY_SOURCE 			= bui-fakekey-$(BUI_FAKEKEY_VERSION).tar.gz
BUI_FAKEKEY_SITE 			= http://$(BR2_SOURCEFORGE_MIRROR).dl.sourceforge.net/sourceforge/beaglebox/
BUI_FAKEKEY_LIBTOOL_PATCH 	= NO
BUI_FAKEKEY_INSTALL_STAGING = YES
BUI_FAKEKEY_DEPENDENCIES 	= bui-lib xlib_libXtst
BUI_FAKEKEY_CONF_OPT 		= --enable-expat

ifeq ($(BR2_PACKAGE_X11R7_LIBXCOMPOSITE),y)
ifeq ($(BR2_PACKAGE_X11R7_LIBXPM),y)
  BUI_FAKEKEY_CONF_OPT+=--enable-composite
  BUI_FAKEKEY_DEPENDENCIES+=xlib_libXcomposite
  BUI_FAKEKEY_DEPENDENCIES+=xlib_libXpm
endif
endif

define BUI_FAKEKEY_DO_AUTOGEN
cd $(BUI_FAKEKEY_DIR) && ./autogen.sh
endef

define BUI_FAKEKEY_POST_CONFIGURE_FIXES
 $(SED) 's:-I[^$$].*/usr/include/freetype2:-I/usr/include/freetype2:' $(STAGING_DIR)/usr/lib/pkgconfig/libmb.pc
 $(SED) 's:^SUBDIRS = fakekey src tests.*:SUBDIRS = fakekey src:g' $(BUI_FAKEKEY_DIR)/Makefile
endef

BUI_FAKEKEY_POST_EXTRACT_HOOKS   += BUI_FAKEKEY_DO_AUTOGEN
BUI_FAKEKEY_POST_CONFIGURE_HOOKS += BUI_FAKEKEY_POST_CONFIGURE_FIXES

#############################################################

$(eval $(call AUTOTARGETS,package/bui,bui-fakekey))
