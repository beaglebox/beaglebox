################################################################################
#
# xdriver_xf86-video-omapfb -- video driver for framebuffer device
#
################################################################################

# XDRIVER_XF86_VIDEO_OMAPFB_VERSION = 0.1.1
XDRIVER_XF86_VIDEO_OMAPFB_VERSION = master
XDRIVER_XF86_VIDEO_OMAPFB_SITE = http://cgit.pingu.fi/xf86-video-omapfb/snapshot/
XDRIVER_XF86_VIDEO_OMAPFB_SOURCE = xf86-video-omapfb-$(XDRIVER_XF86_VIDEO_OMAPFB_VERSION).tar.gz
XDRIVER_XF86_VIDEO_OMAPFB_AUTORECONF = YES
XDRIVER_XF86_VIDEO_OMAPFB_DEPENDENCIES = xserver_xorg-server xproto_fontsproto xproto_randrproto xproto_renderproto xproto_videoproto xproto_xproto
XDRIVER_XF86_VIDEO_OMAPFB_CONF_OPT = --enable-neon

XDRIVER_XF86_VIDEO_OMAPFB_INSTALL_TARGET_OPT = DESTDIR=$(TARGET_DIR) install

$(eval $(call AUTOTARGETS,package/x11r7,xdriver_xf86-video-omapfb))
